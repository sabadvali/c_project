#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#pragma warning(disable : 4996).

using namespace std;

struct TIME
{
	short hr;
	short min;
	short sec;
	short msec;
};


struct DATE
{
	int y;
	int m;
	int d;
};

struct DATETIME :public DATE, public TIME
{

};


struct FLAG
{
	char f32[32];	//gef32.png
	char f64[32];	//gef64.png
	char f128[32];	//gef128.png
	DATE a;
};


struct CITY
{	
	char ic[32];	//geo
	DATE fdate;
	char name[128];
	__int64 population;
	float lat;
	float lngt;

};

struct COUNTRY
{
	DATE fdate;
	DATE pdate;
	TIME gtime;



	char name[128];	//Georgia
	int fcode;		//995
	char ic[32];	//geo
	float population;
	float lat;
	float lngt;

	CITY* cities;


	COUNTRY(int citycount)
	{
		memset(&name, 0, sizeof(name));
		cities = new CITY[citycount];
	}

	~COUNTRY()
	{
		delete[] cities;
	}
};



void main()
{
	COUNTRY g(3000);
	int parameter = -1;

	while (parameter != 0)
	{
		system("cls");
		printf("Chose Parameter: \n0: Exit \n1: Add Country \n2: Add City \n3: Delete \n4: Edit \n5: view \n\n");
		scanf("%d", &parameter);
		switch (parameter)
		{
			case 1:  // add city
			{
				system("cls");
				char country_name[128];
				int fcode;
				char Ic[32];
				float population;
				float lat;
				float lnght;

				printf("Enter Country Name: ");
				scanf("%s", country_name);
				printf("Enter Country Fcode: ");
				scanf("%d", &fcode);
				printf("Enter Country Ic: ");
				scanf("%s", Ic);
				printf("Enter Population. If you Dont Know Please Enter 0: ");
				scanf("%f", &population);
				printf("Enter Latitude. If you Dont Know Please Enter 0: ");
				scanf("%f", &lat);
				printf("Enter Lenght. If you Dont Know Please Enter 0: ");
				scanf("%f", &lnght);

				strcpy(g.name, country_name);
				g.fcode = fcode;
				strcpy(g.ic, Ic);
				g.lat = lat;
				g.lngt = lnght;

				FILE* hf = fopen("Country.txt", "a+");
				fwrite(&g, sizeof(g), 1, hf);
				fclose(hf);

				break;
			}

			case 2: // add city
			{
				system("cls");
				char city_name[128];
				int year;
				int month;
				int day;
				float latitude;
				float lenght;
				__int64 popul;
				char ic_city[32];

				printf("Enter city Name: ");
				scanf("%s", city_name);
				printf("Enter Ic: ");
				scanf("%s", &ic_city);
				printf("Enter Year: ");
				scanf("%d", &year);
				printf("Enter Month: ");
				scanf("%d", &month);
				printf("Enter day: ");
				scanf("%d", &day);
				printf("Enter latitude: ");
				scanf("%f", &latitude);
				printf("Enter Lenght: ");
				scanf("%f", &lenght);
				printf("Enter Population: ");
				scanf("%lld", &popul);

				strcpy(g.cities[0].name, city_name);
				strcpy(g.cities[0].ic, ic_city);
				g.cities[0].fdate.y = year;
				g.cities[0].fdate.m = month;
				g.cities[0].fdate.d = day;
				g.cities[0].lat = latitude;
				g.cities[0].lngt = lenght;
				g.cities[0].population = popul;

				FILE* hf = fopen("Cities.txt", "a+");
				fwrite(&g.cities[0], sizeof(g.cities[0]), 1, hf);
				fclose(hf);

				break;
			}

		}

	}

	
	//test

}




/*
strcpy(g.cities[0].name, "Tbilisi");
g.cities[0].fdate.y = 100;
g.cities[0].fdate.m = 3;
g.cities[0].fdate.d = 4;
g.cities[0].lat = 42.22342234;
g.cities[0].lngt = 42.6756;
g.cities[0].population = 1234000;

FILE* hf = fopen("gcities.txt", "a+");
fwrite(&g.cities[0], sizeof(g.cities[0]), 1, hf);
fclose(hf);

strcpy(g.cities[0].name, "Kutaisi");
g.cities[1].fdate.y = -3100;
g.cities[1].fdate.m = 3;
g.cities[1].fdate.d = 4;
g.cities[1].lat = 42.1222234;
g.cities[1].lngt = 42.27456;
g.cities[1].population = 234000;

hf = fopen("gcities.txt", "a+");
fwrite(&g.cities[1], sizeof(g.cities[1]), 1, hf);
fclose(hf);

*/